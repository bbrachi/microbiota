#!/bin/bash

#PBS -N swarm_pipeline
#PBS -d /group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/
#PBS -S /bin/bash
#PBS -l nodes=1:ppn=1
#PBS -l mem=50gb
#PBS -l walltime=50:00:00
#PBS -o /group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/logs/swarm_pipeline_16S.log.txt
#PBS -e /group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/logs/swarm_pipeline_16S.error.txt
#PBS -M bbrachi@uchicago.edu
#PBS -m e

module add python
module add blast
wd="/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/swarm"
#wd="/Volumes/HD/sweden/microbial_community/swarm"

gene="16S"

fastas=$wd/$gene/fastas
derep=$wd/$gene/dereplicated.fasta
stats=$wd/$gene/swarm_out/otus.stats
swarms=$wd/$gene/swarm_out/otus.swarms
struct=$wd/$gene/swarm_out/otus.struct
repfna=$wd/$gene/swarm_out/representative_seq.fasta
otu_table=$wd/$gene/OTU_table.csv
mkdir $wd/$gene/swarm_out/

##remove fastas of size 0

find $wd/$gene/fastas -size  0 -print0 | xargs -0 rm

##dereplicate fastas at the study level

files=$(ls $wd/$gene/fastas/*.dereplicated.fasta)

export LC_ALL=C
cat $files | \
    awk 'BEGIN {RS = ">" ; FS = "[_\n]"}
     {if (NR != 1) {abundances[$1] += $2 ; sequences[$1] = $3}}
     END {for (amplicon in sequences) {
         print ">" amplicon "_" abundances[amplicon] "_" sequences[amplicon]}}' | \
    sort --temporary-directory=$(pwd) -t "_" -k2,2nr -k1.2,1d | \
    sed -e 's/\_/\n/2'  \
        > $derep

##run swarm

swarm -t 4 -c 20000 -f -i $struct -s $stats -w $repfna  $derep > $swarms

##make a OTU count table

~/bin/build_OTU_table.py -o $otu_table -f $fastas -s $swarms

##assign taxonomy to OTUs

##prepare the repfna file by removing OTUs with abundance <=50

module add qiime
subrepfna=$(echo $repfna | sed 's/_seq/_seq_sub/g')

cat $repfna | \
    awk 'BEGIN {RS = ">" ; FS = "[_\n]"}
   {if($2>=50)print ">OTU" NR-1 "_" $2 "_"$3}' | \
    sed -e 's/\_/\n/2' > $subrepfna

if [[ "$gene" == '16S' ]]; then
    db="/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/swarm/tax/SILVA/Silva_119_rep_set99_aligned_16S_only.fna"
    tt="/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/swarm/tax/SILVA/taxonomy_99_7_levels_consensus.txt"
    #db="/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/swarm/tax/GG/gg_13_8_otus/rep_set/99_otus.fasta"
    #tt="/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/swarm/tax/GG/gg_13_8_otus/taxonomy/99_otu_taxonomy.txt"
    assign_taxonomy.py -i $subrepfna -r $db -t $tt -o $wd/$gene/swarm_out -m uclust
fi
if [[ "$gene" == 'ITS' ]]; then
    db="/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/swarm/tax/UNITE/UNITEv6_sh_99.fasta"
    tt="/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/swarm/tax/UNITE/UNITEv6_sh_99.tax"
    assign_taxonomy.py -i $subrepfna -r $db -t $tt -o $wd/$gene/swarm_out -m blast
fi



##for my laptop because qiime doesn't work on tarbell anymore...
wd="/Volumes/HD/sweden/microbial_community/swarm"
gene="16S"
fastas=$wd/$gene/fastas
derep=$wd/$gene/dereplicated.fasta
stats=$wd/$gene/swarm_out/otus.stats
swarms=$wd/$gene/swarm_out/otus.swarms
struct=$wd/$gene/swarm_out/otus.struct
repfna=$wd/$gene/swarm_out/representative_seq.fasta
otu_table=$wd/$gene/OTU_table.csv

subrepfna=$(echo $repfna | sed 's/_seq/_seq_sub/g')

if [[ "$gene" == '16S' ]]; then
    db="/Volumes/HD/sweden/microbial_community/swarm/tax/SILVA/Silva_119_rep_set99_aligned_16S_only.fna"
    tt="/Volumes/HD/sweden/microbial_community/swarm/tax/SILVA/taxonomy_99_7_levels_consensus.txt"
    #db="/Volumes/HD/sweden/microbial_community/swarm/tax/GG/gg_13_8_otus/rep_set/99_otus.fasta"
    #tt="/Volumes/HD/sweden/microbial_community/swarm/tax/GG/gg_13_8_otus/taxonomy/99_otu_taxonomy.txt"
    assign_taxonomy.py -i $subrepfna -r $db -t $tt -o $wd/$gene/swarm_out -m blast
fi
if [[ "$gene" == 'ITS' ]]; then
    db="/Volumes/HD/sweden/microbial_community/swarm/tax/UNITE/UNITEv6_sh_99.fasta"
    tt="/Volumes/HD/sweden/microbial_community/swarm/tax/UNITE/UNITEv6_sh_99.tax"
    assign_taxonomy.py -i $subrepfna -r $db -t $tt -o $wd/$gene/swarm_out -m blast
fi




