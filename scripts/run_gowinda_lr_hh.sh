
#wd="/home/benjamin/Documents/sweden/microbiota"

##macbook pro
wd="/Volumes/HD/sweden/microbiota"
std="/Volumes/HD/sweden/genomes/"
jar_path="/Users/ben/jar/"


##dell
##wd="/home/benjamin/Documents/sweden/microbiota"
##std="/home/benjamin/Documents/sweden/genomes"
##jar_path="/home/benjamin/jar/"

##define files
gff=$std/TAIR10_GFF3_genes_transposons.gff
gtf=$std/TAIR10_GFF3_genes_transposons.gtf
Tot_gene=$std/total_genes.txt

total_snps=$wd/res/map_gowinda.tsv
cand_snps=$wd/res/top_lr_hh_gowinda.tsv

res=$wd/res/go_enrichment_lr_hh.txt

goasso=$std/association_gominer.txt

##make the gff into a gtf

python ~/source/Gff2Gtf.py --input $gff  > $gtf

##remove the Chr of the chromosome denomination from the $gtf file
sed -i -e 's/Chr//g' $gtf

##make the total gene list
cat $gtf | awk '{print $10}'|sed 's/";//'|sed 's/"//'|sort |uniq > $tot_gene

##follow instructions on the gowinda website to get the files from gominer and unzip the file

##using all evidence code for go annotation

##check the parameters:
job_number=1897406140

cat $std/work$job_number/userinputparam.txt

python ~/source/GoMiner2FuncAssociate.py --input $std/work$job_number/total_genes.txt$job_number.dir/total_genes.txt$jobnumber.dir/total_genes.txt.change.gce > $goasso

cd $wd

res=$wd/res/go_enrichment_lr_hh.txt
res2=$(echo $res | sed 's/go_enrichment/revigo/g')

java -Xmx4g -jar $jar_path/Gowinda-1.12.jar  --snp-file $total_snps --candidate-snp-file $cand_snps --gene-set-file $goasso --annotation-file $gtf --simulations 100000 --min-significance 0.05 --threads 7 --output-file $res --mode gene --min-genes 1 --gene-definition updownstream1000

##and for some reason my java or gowinda uses comas to separate decimals so we can change that as we select the annotations and qvalues for imputing in REVIGO

cat $res | cut -f1,5 | sed "s/,/./g" > $res2




