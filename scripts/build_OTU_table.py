#! /usr/bin/env python

import sys
import getopt
import time
import datetime
import math
import os
import sqlite3
from re import sub
from subprocess import call
import re
from pandas import *


class prog():

    def __init__(self):
        self.fastas = ""
        self.swarm_out = ""
        
    def help(self):
        """
        Display the help on stdout.

        The format complies with help2man (http://www.gnu.org/s/help2man)
        """
        msg = "`%s' This script builds the OTU count table from the output of Swarm and dereplicated fasta files.\n"
        msg += "\n"
        msg += "Usage: %s [OPTIONS] ...\n" % os.path.basename(sys.argv[0])
        msg += "\n"
        msg += "Options:\n"
        msg += "  -h, --help\tdisplay the help and exit\n"
        msg += "  -V, --version\toutput version information and exit\n"
        msg += "  -v, --verbose\tverbosity level (0/default=1/2/3)\n"
        msg += "  -o, --output\tname of the output file (absolute path please!). The ouput is a csv file\n"
        msg += "  -f, --fastas\t the folder where the dereplicated fasta files are located (full path!!)\n"
        msg += "  -s, --swarms\t the output from swarm (the one grouping reads into OTUs)\n"
        msg += "\n"
        msg += "Examples:\n"
        msg += "  %s -o /Users/ben/Documents/data/fastas/count_table.csv -f /Users/ben/Documents/data/fasta -s /Users/ben/Documents/swarm.output" % os.path.basename(sys.argv[0])
        msg += "\n"
        print msg; sys.stdout.flush()

    def version(self):
        """
        Display version and license information on stdout.
        """
        msg = "%s 1.0\n" % os.path.basename(sys.argv[0])
        msg += "\n"
        msg += "Written by Benjamin Brachi.\n"
        msg += "\n"
        # choose between:
        msg += "Not copyrighted -- provided to the public domain\n"
        # or:
        # msg += "Copyright (C) 2011-2013 Timothee Flutre.\n"
        # msg += "License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n"
        # msg += "This is free software; see the source for copying conditions.  There is NO\n"
        # msg += "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"
        print msg; sys.stdout.flush()


    def setAttributesFromCmdLine(self):
        """
        Parse the command-line arguments.
        """
        self.output=""
        self.verbose=0
        self.swarms=""
        self.fastas=""
        try:
            opts, args = getopt.getopt( sys.argv[1:], "hVv:o:s:f:",["help", "version","verbose", "output", "swarms", "fastas"])
        except getopt.GetoptError, err:
            sys.stderr.write("%s\n" % str(err))
            self.help()
            sys.exit(2)
        for o, a in opts:
            if o == "-h" or o == "--help":
                self.help()
                sys.exit(0)
            elif o == "-V" or o == "--version":
                self.version()
                sys.exit(0)
            elif o == "-v" or o == "--verbose":
                self.verbose = int(a)
            elif o == "-o" or o == "--output":
                self.output = str(a)
            elif o == "-s" or o == "--swarms":
                self.swarms = str(a)
            elif o == "-f" or o == "--fastas":
                self.fastas = str(a)
            else:
                assert False, "invalid option"

    def checkAttributes(self):
        """
        Check the values of the command-line parameters.
        """
        if self.output == "":
            msg = "Please provide the directory including fasta files with -o or --output."
            sys.stderr.write("%s\n\n" % msg)
            self.help()
            sys.exit(1)
        if self.swarms == "":
            msg = "Please provide the path to a swarm output -s or --swarms."
            sys.stderr.write("%s\n\n" % msg)
            self.help()
            sys.exit(1)
        if not os.path.exists(self.fastas):
            msg = "ERROR: can't find file %s" % self.fastas
            sys.stderr.write("%s\n\n" % msg)
            self.help()
            sys.exit(1)

                        

                
    def main(self):
        def listdir_fullpath(d):
            return [os.path.join(d, f) for f in os.listdir(d)]
        fastas=listdir_fullpath(self.fastas) 
        swarm=open(self.swarms, "r")
        dictotu={'OTU':[], 'seq':[]}
        count=1
        OTUs=[]
        for w in swarm:
            a=re.sub("[_][0-9]+", "", w).strip("\n").split(" ")
            for r in a:
                dictotu['OTU'].append("OTU"+"{:0>6d}".format(count))
                dictotu['seq'].append(r)
            OTUs.append("OTU"+"{:0>6d}".format(count))
            count=count+1
        dfo=DataFrame(dictotu)
        swarm.close()
        ##build the count table sample by sample
        counttable=DataFrame(index=OTUs)
        for file in fastas:
            s=file.split("/")
            sample=sub("-", "_", sub(".dereplicated.fasta", "", s[len(s)-1]))
            dictsample={'seq':[],sample:[]}
            fasta=open(file, "r")
            for seq in fasta:
                if seq[0]==">":
                    e=seq.strip("\n").strip(">").split("_")
                    dictsample['seq'].append(e[0])
                    dictsample[sample].append(int(e[1]))
            dfs=DataFrame(dictsample)
            df=merge(dfo, dfs, right_on='seq', left_on='seq', how='outer')
            x=df.groupby('OTU')[sample].sum()
            counts=DataFrame(x.fillna(0))
            counttable[sample]=counts
        counttable.to_csv(self.output)

               
    def run(self):
        self.checkAttributes()
        if self.verbose > 0:
            msg = "START %s" % time.strftime("%Y-%m-%d %H:%M:%S")
            startTime = time.time()
            msg += "\ncmd-line: %s" % ' '.join(sys.argv)
            msg += "\ncwd: %s" % os.getcwd()
            sys.stderr.write(msg+"\n")
        self.main()
        if i.verbose > 0:
            msg = "END %s %s" % (os.path.basename(sys.argv[0]),
            time.strftime("%Y-%m-%d %H:%M:%S"))
            endTime = time.time()
            runLength = datetime.timedelta(seconds=math.floor(endTime - startTime))
            msg += " (%s)" % str(runLength)
            sys.stderr.write(msg+"\n")
    

if __name__ == "__main__":
        i = prog()
        i.setAttributesFromCmdLine()
        i.run()


