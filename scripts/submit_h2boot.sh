#!/bin/bash

wd="/group/bergelson-lab/bbrachi_data/sweden/microbiota"

for gene in "ITS" "16S"
do
    for e in "ull" "rat" "ram" "ada"
    do
	for y in 2012 2013
	do
	    for slice in $(seq 1 40)
	    do
		echo "
#!/bin/bash
#PBS -N booth2_"$e"_"$y"_"$gene"
#PBS -d "$wd"
#PBS -S /bin/bash
#PBS -l nodes=1:ppn=8
#PBS -l mem=8gb
#PBS -l walltime=1:00:00
#PBS -o "$wd"/logs/booth2_"$gene"_"$e"_"$y".log
#PBS -e "$wd"/logs/booth2_"$gene"_"$e"_"$y".err
module add R
Rscript --vanilla "$wd"/scripts/boot_rep_batch.R "$gene" "$e" "$y" "$slice" > "$wd"/logs/R_output_boot_rep_batch_"$gene"_"$e"_"$y".txt" > $wd"/submit_h2boot.sh"
	    qsub $wd"/submit_h2boot.sh"
	    rm $wd"/submit_h2boot.sh"
	    sleep 0.2s
	    done
	done
    done
done

