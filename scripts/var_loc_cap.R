library(vegan)
## ---- cap
genes=c("ITS", "16S")

for(gene in genes){
    ct=readRDS(file=paste("./data/ct_no_outliers_", gene, ".rds", sep=""))
    s=readRDS(file=paste("./data/s_no_outliers_", gene, ".rds", sep=""))
    s$year=as.factor(s$year)
    #tot=sum(ct)
                                        #rs=colSums(ct)
    #ct=ct[, rs>=(0.0001*tot)]
    nct=t(apply(ct, 1, function(x, depth){y=round(x*(depth/sum(x)))}, depth=1000)) ##normalization according to Kurtz et al.
    nct=log(nct+1)
    cap <- capscale(nct ~ exp + year + exp*year , s, dist="bray") ##cap
    saveRDS(cap, paste("./res/capscale_all_", gene, ".rds", sep=""))
    #a=anova(cap, parallel=3) ##anova
    scap=summary(cap)
    saveRDS(scap, paste("./res/summary_capscale_all_", gene, ".rds", sep=""))
    #saveRDS(a, paste("./res/anova_capscale_all_", gene, "_nolog.rds", sep=""))
    #cap2 <- capscale(nct ~ 1 , dist="bray") ##regular pcoa
    #saveRDS(cap2, paste("./res/PcoA_all_", gene, "_nolog.rds", sep=""))
}
## ---- end-of-cap

