#!/bin/bash

gene=$1
echo $gene

if [[ "$gene" == '' ]]; then
    echo "Usage 'submit_process_reads_batch.sh gene' where gene is either ITS or 16S"
fi
if [[ "$gene" == 'ITS' ]]; then
    flowcells=(A9VVJ A9W58 A9V7D A9VF8 A9VUR A9W55 ABD38 AB9L4 AB9WP ABC6T AB9U1)
    years=(2013 2013  2013  2013  2013  2013  2012  2012  2012  2012  2012)
    genes=(ITS ITS ITS ITS ITS ITS ITS ITS ITS ITS ITS)
 fi
if [[ "$gene" == '16S' ]]; then
    flowcells=(AECNJ AEB8T ADHKE AEHUF AEHY0 AEHV3 AEJN8 AFD3K AG58D AGRB3 AJWA7)
    genes=(16S 16S 16S 16S 16S 16S 16S 16S 16S 16S 16S)
    years=(2013 2013 2013 2013 2013 2013 2012 2012 2012 2012 2012)
fi

src=/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/runs
wd="/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/swarm"
##set number of samples per jobs
N=25
##walltime, sorts of dependant on the previous parameter
WT="4:00:00"

mkdir $wd/submits
L=${#flowcells[@]}
count=0
while [ $count -lt $L ];
do
    fastqsFolder=$src/*-${flowcells[$count]}/Data/Intensities/BaseCalls/
    R1=($(ls $fastqsFolder/*-*-*R1_001.fastq.gz))
    LR1=${#R1[@]}
    starts=($(seq 0 $N $LR1))
    NJOBS=${#starts[@]}
    countjobs=0
    while [ $countjobs -lt $NJOBS ]
    do
	echo "
#!/bin/bash
#PBS -N process_reads_swarm_${flowcells[$count]}_$countjobs
#PBS -d /group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/
#PBS -S /bin/bash
#PBS -l nodes=1:ppn=1
#PBS -l mem=4gb
#PBS -l walltime="$WT"
#PBS -o /group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/logs/process_reads_swarm_${flowcells[$count]}_$countjobs.log
#PBS -e /group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/logs/process_reads_swarm_${flowcells[$count]}_$countjobs.error
~/bin/process_reads_swarm.sh 0 "${flowcells[$count]}" "${years[$count]}" "${genes[$count]}" "$wd" "${starts[$countjobs]}" "$N"
" > $wd/submits/submit_process_reads.temp
	qsub $wd/submits/submit_process_reads.temp
	countjobs=$((countjobs+1))
    done
    count=$((count+1))
    sleep 0.2s
done


