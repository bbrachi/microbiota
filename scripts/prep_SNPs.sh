module add plink
module add gemma

wd="/group/bergelson-lab/bbrachi_data/sweden/genomes/002.Swedes220.SNPs"
vcf="Swedes220.175k.prior15.gauss4.ts99.5.BIALLELIC.vcf"
microbiota="/group/bergelson-lab/bbrachi_data/sweden/microbiota/GWA/snps"

plink --vcf $wd/$vcf --make-bed --out $wd/sweden --missing-genotype N --chr 1-5  --allow-extra-chr --double-id

head -n 200 $wd/sweden.fam > $wd/keep.txt
head $wd/keep.txt
tail $wd/keep.txt

mkdir $microbiota
recodeAT=$wd/recodeAT/sweden_200
outbed=$wd/bed/sweden_200
bimbam=$wd/bimbam/sweden_200
mkdir $wd/recodeAT
mkdir $wd/bed
mkdir $wd/bimbam

##remove accessions we don't need (keep the first 200 genotypes)

plink --bfile $wd/sweden --make-bed --allow-no-sex --keep $wd/keep.txt --missing-code N,-9,0,NA --no-pheno -out $wd/sweden_200

##filter out rare SNPs

plink --bfile $wd/sweden_200 --allow-no-sex --maf 0.05 --missing-code N,-9,0,NA --make-bed --out $wd/sweden_200_filt1

##remove poorly genotyped SNPs

plink --bfile $wd/sweden_200_filt1 --allow-no-sex --geno 0.05 --missing-code N,-9,0,NA --make-bed --out $wd/sweden_200_filt2

##filter out rare SNPs again write bed (for bayesR) and recode (for gemma)

plink --bfile $wd/sweden_200_filt2 --allow-no-sex -maf 0.05 --missing-code N,-9,0,NA --no-pheno --make-bed --set-missing-var-ids @_# --out $outbed

##make a freq file

plink --bfile $outbed --freq --out $outbed

plink --bfile $wd/sweden_200_filt2 --allow-no-sex -maf 0.05 --missing-code N,-9,0,NA --no-pheno -recode A-transpose --set-missing-var-ids @_# --out $recodeAT


##to make it into a mean genotype file, it looks like I just need to remove the centimorgan column

tail -n +2 $recodeAT.traw | awk -F "\t" '{print $1"_"$4"\t"$4"\t"$1}'| sed 's/\t/,/g' > $bimbam.bimbam.map
cat $bimbam.bimbam.map | awk -F "," '{print $1}' > $wd/snps_list.temp
tail -n +2 $recodeAT.traw | cut -f5- | sed 's/\t/,/g' >  $wd/bimbam.temp

paste -d "," $wd/snps_list.temp $wd/bimbam.temp > $bimbam.bimbam.geno
rm $wd/snps_list.temp
rm $wd/bimbam.temp

head -n 1 $recodeAT.traw | cut -f7- | sed 's/[_][0-9]*/\n/g' > $bimbam.bimbam.acclist

##check the results
head $bimbam.bimbam.geno
head $bimbam.bimbam.map
head $bimbam.bimbam.acclist

## how many genotypes in the snps file:
head -n 1 $bimbam.bimbam.geno | awk -F "," '{print NF-3}' ##should say 200
## how many accessions in the list of accessions: should say the same + 1 because there is an empty line
wc -l $bimbam.bimbam.acclist ## 201

## over to the microbiota analysis folder.

cp $bimbam.bimbam.map $microbiota
cp $bimbam.bimbam.geno $microbiota
cp $bimbam.bimbam.acclist $microbiota

cp $outbed* $microbiota
##in the microbiota folder, make a backup fam file
cp $microbiota/sweden.fam $microbiota/sweden.fam.backup

## make kinship matrices with all 200 genotypes
for i in $(seq 1 200); do echo 1; done >  $wd/fakephen.txt

cd $wd
gemma -gk 1 -g $bimbam.bimbam.geno -p $wd/fakephen.txt -o K_all_accession
gemma -gk 2 -g $bimbam.bimbam.geno -p $wd/fakephen.txt -o K_all_accession

