##investigate the genetic correlations among heritable hubs
## ---- blups

library(plyr)
library(mixOmics)
library(lme4)
library(igraph)
library(Hmisc)
library(coxme)
source("./scripts/heritability_functions.R")

m=readRDS("./res/heritable_hubs_v2.rds")

for(i in 1:nrow(m)){
    if(substring(m[i, 1], 1, 1)=="B"){gene="16S"}else{gene="ITS"}
    ct=readRDS(file=paste("./data/ct_no_outliers_", gene, ".rds", sep=""))
    s=readRDS(file=paste("./data/s_no_outliers_", gene, ".rds", sep=""))
    s$year=as.factor(s$year)
    subct=ct[s$exp==m[i, "exp"] & s$year==m[i, "year"],]
    subs=droplevels(s[s$exp==m[i, "exp"] & s$year==m[i, "year"],])
    tot=sum(subct)
    rs=colSums(subct)
    subct=subct[, rs>=(0.0001*tot)]
    lsubct=logratio.transfo(subct+1, logratio="CLR")
    x=lsubct[[paste(m[i, "OTU"])]]
    ##compute blups per accession
    mod=lmer(x~(1|subs$id))
    b=ranef(mod)[[1]]
    if(i==1){blups=data.frame(id=row.names(b), x=b[,1]); colnames(blups)[2]=paste(m[i, 1], "_", m[i, "exp"], "_", m[i, "year"],  sep="")
    }else{temp=data.frame(id=row.names(b), x=b[,1]); colnames(temp)[2]=paste(m[i, 1], "_", m[i, "exp"], "_", m[i, "year"], sep=""); blups=merge(blups, temp, by="id", all=T)}
}

saveRDS(blups, "./res/blups_hh_v2.rds")

## ---- end-off-blups

## ---- gencorr
library(igraph)
library(Hmisc)

## compute the correlations among heritable hubs
blups=readRDS("./res/blups_hh_v2.rds")
C=cor(blups[,-1], method="pearson", use="pairwise")
m=readRDS("./res/heritable_hubs_v2.rds")

##make a graph

C=rcorr(as.matrix(blups[,-1]), type=c("pearson"))

adj=C$r
adj[C$P>0.01]=0

g=graph.adjacency(adj, mode="undirected", weighted=T, diag=F)
cols=c("grey10", "grey30","grey50",  "grey70")[match(m$exp,c("ull", "rat", "ada", "ram"))]
#cols=c("firebrick2", "gold","darkblue",  "dodgerblue")[match(m$exp,c("ull", "rat", "ada", "ram"))]
coltext=rep("white", nrow(m))
coltext[m$exp%in%c("ada", "ram")]="black"
shapes=c("circle","square")[match(m$year,c(2012, 2013))]
col_edges=edge_attr(g)$weight
col_edges[col_edges>0]="grey20"
col_edges[col_edges<0]="grey70"



#pdf("./figures/graph_gen_corrs_v2.pdf", paper="special", height=5, width=5, pointsize=8)
jpeg("./figures/graph_gen_corrs_v2.jpeg", res=600, unit="in", height=5, width=5, pointsize=8)
par(mar=c(1, 1, 1, 1))
l=layout_nicely(g)
l[13,]=l[13,]
plot.igraph(g, layout=l,vertex.size=100*m$h2+10, edge.width=abs(edge_attr(g)$weight)*10, vertex.label.dist=0, vertex.label.cex=1.2, vertex.label=paste(m$OTU), vertex.color=cols, vertex.shape=shapes, vertex.label.color=coltext, edge.color=col_edges)
legend("bottomleft", legend=c("S1", "S2", "N1", "N2", "2012", "2013"), col=c("grey10", "grey20","grey40",  "grey50", 1, 1), pt.cex=2, pch=c(rep(16, 4), 1, 0))
legend("bottomleft", legend=c("", "", "", "", "", ""), col=c("white", "white","black",  "black", 1, 1), pt.cex=0.6, pch=c(rep("A", 4), "", "") ,bty="n")
dev.off()


## ---- gencorr

