#!/bin/bash

gene=$1

src=/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/runs

if [[ "$gene" == '' ]]; then
    echo "Usage 'submit_process_reads_batch.sh gene' where gene is either ITS or 16S" 
fi
if [[ "$gene" == 'ITS' ]]; then
    flowcells=(A9VVJ A9W58 A9V7D A9VF8 A9VUR A9W55 ABD38 AB9L4 AB9WP ABC6T AB9U1)
    years=(2013 2013  2013  2013  2013  2013  2012  2012  2012  2012  2012)
    genes=(ITS ITS ITS ITS ITS ITS ITS ITS ITS ITS ITS)
 fi
if [[ "$gene" == '16S' ]]; then
    flowcells=(AECNJ AEB8T ADHKE AEHUF AEHY0 AEHV3 AEJN8 AFD3K AG58D AGRB3 AJWA7)
    genes=(16S 16S 16S 16S 16S 16S 16S 16S 16S 16S 16S)
    years=(2013 2013 2013 2013 2013 2013 2012 2012 2012 2012 2012)
fi

wd="/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/swarm"


echo "original fastqs and file names:"
L=${#flowcells[@]}
count=0
total=0
names=()
while [ $count -lt $L ];
do
    fastqsFolder=$src/*-${flowcells[$count]}/Data/Intensities/BaseCalls/
    R1=($(ls $fastqsFolder/*-*-*R1_001.fastq.gz))
    for file in ${R1[@]}
    do
	names+=" ${file##*/}"
    done
    LR1=${#R1[@]}
    echo ${flowcells[$count]} $LR1 
    u=($(echo ${R1[@]} | tr ' ' '\n' | sort -u | tr '\n' ' ') )
    echo "unique sample names ="${#u[@]} 
    count=$((count+1))
    total=$((total+$LR1))
done
echo "total="$total
names=($names)
unique_names=($(echo ${names[@]} | tr ' ' '\n' | sort -u | tr '\n' ' ') )
echo "total number of unique sample names ="${#unique_names[@]} 

# ##now how many dereplicated samples 

#echo "dereplicated files:"
#files=($(ls $wd/$gene/*/fasta/*.dereplicated.fasta))
#echo "total number of dereplicated files= ${#files[@]}"

count=0
total=0
names=()
while [ $count -lt $L ];
do
    derep=($(ls $wd/$gene/${flowcells[$count]}/fasta/*.dereplicated.fasta))
    for file in ${derep[@]}
    do
	names+=" ${file##*/}"
    done
    LD=${#derep[@]}
    echo ${flowcells[$count]} $LD
    count=$((count+1))
    total=$((total+$LD))
done
echo total number of dereplicated fasta = $total
unique_names=($(echo ${names[@]} | tr ' ' '\n' | sort -u | tr '\n' ' ') )
echo "total number of unique sample names ="${#unique_names[@]} 

