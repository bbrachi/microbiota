**Microbial community analysis**

Analyze both bacterial and fungal community data from the paper Brachi et al. 
Please refer of main_microbiota_analysis.Rmd/.pdf for further details about the analysis.
